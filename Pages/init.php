<?php
ob_start();
include('Config/DbConnection.php');
include('Controller/ManageTranslations.php');
include('Controller/ManageTests.php');

use Translate\Config\DbConnection;
use Translate\Controller\ManageTranslations;
use Translate\Controller\ManageTests;

$dbConnection = new DbConnection();
$manager = New ManageTranslations($dbConnection);
$testManager = New ManageTests($dbConnection, $manager);
?>
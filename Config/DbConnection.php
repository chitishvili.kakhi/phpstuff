<?php
/**
 * @package Translate
 */

declare(strict_types=1);

namespace Translate\Config;

use mysqli;
use PDO;

/**
 * Class DbConnection
 *
 * @package Translate\Config
 */
class DbConnection
{
    /**
     * @var string
     */
    const DB_USERNAME = 'root';

    /**
     * @var string
     */
    const DB_PASSWORD = '';

    /**
     * @var string
     */
    const DB_NAME = 'translationtests';

    /**
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * @var mixed
     */
    public $connection;

    /**
     * DbConnection constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->createConnection();
    }

    /**
     * Get Connection Method
     *
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Execute Query
     *
     * @param string $query
     *
     * @return mixed
     */
    public function executeQuery(string $query)
    {
        $data = $this->connection->query($query);

        return $data->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Escape String
     *
     * @param $string
     *
     * @return mixed
     */
    public function stringEscape($string)
    {
        return addslashes($string);
    }

    /**
     * Create Connection Method
     *
     * @return void
     */
    protected function createConnection()
    {
        $host = self::DB_HOST;
        $database = self::DB_NAME;

        $this->connection = new PDO(
            "mysql:host=$host; dbname=$database",
            self::DB_USERNAME,
            self::DB_PASSWORD
        );
    }
}

<?php
/**
 * @package Translate
 */

declare(strict_types=1);

namespace Translate\Controller;

use http\Params;
use Translate\Config\DbConnection;
use Translate\Controller\ManageTranslations;

/**
 * Class ManageTests
 *
 * @package Translate\Controller
 */
class ManageTests
{
    /**
     * @var int
     */
    const TEST_QUESTION_NUMBER = 5;

    /**
     * @var DbConnection
     */
    protected $connection;

    /**
     * @var ManageTranslations
     */
    protected $translationManager;

    /**
     * ManageTranslations constructor.
     *
     * @param DbConnection $connection
     * @param ManageTranslations $translationManager
     */
    public function __construct(
        DbConnection $connection,
        ManageTranslations $translationManager
    )
    {
        $this->connection = $connection;
        $this->translationManager = $translationManager;

        $this->evaluateSubmission();
    }

    /**
     * Generate Test Method
     *
     * @return array
     */
    public function generateTest(): array
    {
        $testOptions = $this->getAllTranslations();
        $counter = 0;
        $testArray = [];

        while ($counter < self::TEST_QUESTION_NUMBER) {
            shuffle($testOptions);
            $question = array_pop($testOptions);
            $answerOptions = array_column($testOptions, 'georgian');
            $questionStructure = [
                'question' => $question,
                'options' => [
                    $answerOptions[0],
                    $answerOptions[1],
                    $answerOptions[2],
                    $question['georgian']
                ]
            ];

            array_push($testArray, $questionStructure);

            $counter++;
        }

        return $testArray;
    }

    /**
     * Get All Translations inside of an array
     *
     * @return array
     */
    protected function getAllTranslations(): array
    {
        return $this->translationManager->getTranslationList();
    }

    /**
     * Evaluate Submissions Method
     * Gets data from session, checks agaisnt correct answers
     * sets end time then pushes to DB
     *
     * @return void
     */
    protected function evaluateSubmission()
    {
        $score = 0;

        if (isset($_POST['testSubmit'])) {
            $data = $_POST;
            $testTaker = $data['name'];
            $startTime = $data['starttime'];
            $endTime = date('Y-m-d H:i:s');

            foreach ($data as $key => $value) {
                if (array_key_exists($key, $_SESSION)) {
                    if ($value === $_SESSION[$key]) {
                        $score++;
                    }
                }
            }

            $_POST['score'] = $score;

            $this->pushTest([
                'name' => $testTaker,
                'startTime' => (string) $startTime,
                'endTime' => (string) $endTime,
                'score' => (string) $score,
                'timeTaken' => strtotime($endTime) - strtotime($startTime)
            ]);

            session_destroy();
        }
    }

    /**
     * Pushes Test results to the database
     *
     * @param array $data
     *
     * @return void
     */
    protected function pushTest(array $data)
    {
        $connection = $this->connection;
        $query  = "INSERT INTO tests(student_name, score, started, ended, total_time) ";
        $query .= "VALUES(' ";
        $query .= $connection->stringEscape($data['name']) . "', '";
        $query .= $connection->stringEscape($data['score']) . "', '";
        $query .= $connection->stringEscape($data['startTime']) . "', '";
        $query .= $connection->stringEscape($data['endTime']) . "', '";
        $query .= $connection->stringEscape((string) gmdate("H:i:s", (int) $data['timeTaken']) . ' (H:m:s)') . "')";

        $connection->executeQuery($query);
    }
}

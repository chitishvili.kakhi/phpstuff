-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2021 at 03:31 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `translationtests`
--

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `started` varchar(255) DEFAULT NULL,
  `ended` varchar(255) DEFAULT NULL,
  `total_time` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `student_name`, `score`, `started`, `ended`, `total_time`) VALUES
(13, ' afsafsafsa', '1', '2021-10-12 10:59:51', '2021-10-12 10:59:59', '00:00:08 (H:m:s)'),
(14, ' test33', '4', '2021-10-12 11:00:30', '2021-10-12 11:10:04', '00:09:34 (H:m:s)'),
(15, ' asfsaf', '4', '2021-10-12 11:29:00', '2021-10-12 11:29:09', '00:00:09 (H:m:s)'),
(16, ' sg', '4', '2021-11-01 15:29:57', '2021-11-01 15:30:15', '00:00:18 (H:m:s)');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(11) NOT NULL,
  `english` varchar(255) DEFAULT NULL,
  `georgian` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `english`, `georgian`) VALUES
(21, 'a', 'ab'),
(22, 'b', 'b'),
(23, 'fac', 'af'),
(24, 'd', 'd'),
(25, 'e', 'e'),
(26, 'f', 'f'),
(27, 'g', 'g'),
(28, 'h', 'h'),
(29, 'i', 'i'),
(30, 'j', 'j'),
(31, 'k', 'k'),
(32, 'l', 'l'),
(33, 'm', 'm'),
(34, 'n', 'n'),
(35, 'o', 'o'),
(36, 'p', 'p'),
(37, 'q', 'q'),
(38, 'r', 'r'),
(39, 's', 's'),
(40, 't', 't'),
(41, 'u', 'u'),
(42, 'v', 'v'),
(43, 'w', 'w'),
(44, 'x', 'x'),
(45, 'y', 'y'),
(46, 'z', 'z'),
(47, 'zzzz', 'zzzz'),
(48, 'pdo', 'pdo'),
(51, 'bbbbbb', 'bruh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

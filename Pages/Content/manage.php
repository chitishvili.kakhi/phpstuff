<?php
$translations = $manager->getTranslationList();
?>

<?php foreach ($translations as $translation): ?>
    <form action="index.php?action=manage" method="post">
        <input type="text" id="id" name="id" value="<?= $translation['id'] ?>" style="display: none">
        <label for="georgian">Georgian:</label><br>
        <input type="text" id="georgian" name="georgian" value="<?= $translation['georgian'] ?>" placeholder="Georgian"><br>
        <label for="english">English:</label><br>
        <input type="text" id="english" name="english" value="<?= $translation['english'] ?>" placeholder="English"><br><br>

        <input type="submit" value="Save" name="edit">
        <input type="submit" value="Delete" name="delete">
    </form>
    <hr>
<?php endforeach; ?>
<?php
/**
 * @package Translate
 */

declare(strict_types=1);

namespace Translate\Controller;

use Translate\Config\DbConnection;

/**
 * Class AddTranslation
 *
 * @package Translate\controller\AddTranslation
 */
class ManageTranslations
{
    /**
     * @var DbConnection
     */
    protected $connection;

    /**
     * ManageTranslations constructor.
     *
     * @param DbConnection $connection
     */
    public function __construct(
        DbConnection $connection
    )
    {
        $this->connection = $connection;
        $this->checkAndExecuteAction();
    }

    /**
     * Checks what kind of action should be done
     * Constructs query, and executes it
     */
    public function checkAndExecuteAction()
    {
        if (isset($_POST['translationAction'])) {
            $postData = $this->getPostedValues();

            $this->addTranslation($postData['georgian'], $postData['english']);
        } else if (isset($_POST['delete'])) {
            $this->removeTranslation($_POST['id']);
        } else if (isset($_POST['edit'])) {
            $id = $_POST['id'];
            $postData = $this->getPostedValues();

            $this->editTranslation($id, $postData);
        }
    }

    /**
     * Get Posted Values
     *
     * @return array
     */
    protected function getPostedValues(): array
    {
        return [
            'english' => $_POST['english'],
            'georgian' => $_POST['georgian']
        ];
    }

    /**
     * Add Translation Method
     *
     * @param string $georgian
     * @param string $english
     */
    protected function addTranslation(string $georgian, string $english)
    {
        $query  = "INSERT INTO translations(english, georgian) ";
        $query .= "VALUES('";
        $query .= $this->connection->stringEscape($english) . "', '";
        $query .= $this->connection->stringEscape($georgian) . "')";

        $this->connection->executeQuery($query);
    }

    /**
     * Remove Translation Method
     *
     * @param string $id
     */
    protected function removeTranslation(string $id)
    {
        $query = "DELETE FROM translations WHERE id =  " . $this->connection->stringEscape($id);

        $this->connection->executeQuery($query);
    }

    /**
     * Edit Translation Method
     *
     * @param string $id
     * @param array $data
     */
    protected function editTranslation(string $id, array $data)
    {
        $query  = "UPDATE translations ";
        $query .= "SET english = '" . $this->connection->stringEscape($data['english']) . "', ";
        $query .= "georgian = '" . $this->connection->stringEscape($data['georgian']) . "'";
        $query .= " where id = " . $id;

        $this->connection->executeQuery($query);
    }

    /**
     * Get TranslationList
     *
     * @return array
     */
    public function getTranslationList(): array
    {
        $translationList = $this->connection->executeQuery("SELECT * FROM translations");
        $translations = [];

        if (!$translationList) {
            return $translations;
        }

        foreach ($translationList as $translation) {
            array_push(
                $translations,
                [
                    'id' => $translation['id'],
                    'english' => $translation['english'],
                    'georgian' => $translation['georgian']
                ]
            );
        }

        return $translations;
    }

    /**
     * Get Connection Method
     *
     * @return mixed
     */
    protected function getConnection()
    {
        return $this->connection->getConnection();
    }
}

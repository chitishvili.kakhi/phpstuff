<?php
$tests = $testManager->generateTest();
?>
<h1>Taking a Test</h1>
<form action="index.php?testTaken=true" method="post" style="margin-top:50px;">
    <input type="text" id="starttime" name="starttime" style="display: none;" value="<?= date('Y-m-d H:i:s') ?>">
    <label for="name">First Name / Last Name:</label>
    <input type="text" id="name" name="name" required><br><br>
    <?php foreach ($tests as $test): ?>
        <?php
        shuffle($test['options']);
        $_SESSION[$test['question']['english']] = $test['question']['georgian'];
        ?>
        <hr>
        <p>Translation for <?= $test['question']['english'] ?> <strong></strong></p>
        <?php foreach($test['options'] as $option): ?>
            <input type="radio" id="<?= $option ?>" name="<?= $test['question']['english'] ?>" value="<?= $option ?>" required>
            <label for="<?= $option ?>"><?= $option ?></label><br>
        <?php endforeach; ?>
        <hr>
    <?php endforeach; ?>
    <input type="submit" value="submit" name="testSubmit">
</form>

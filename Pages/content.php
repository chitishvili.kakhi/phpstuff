<?php
$action = null;
$defaultInclude = 'Pages/Content/default.php';

if (isset($_GET['action'])) {
    $action = 'Pages/Content/' . $_GET['action'] . '.php';
}
?>
<div class="col-sm-10 mainContainer">
    <div class="row">
        <div class="col-md-12 pageHeader">
            <?php if (isset($_POST['testSubmit'])): ?>
                <h4>Test finished. You scored: <?= $_POST['score'] ?></h4>
            <?php endif; ?>
            <?php $action ? include($action) : include($defaultInclude)  ?>
        </div>
    </div>
</div>